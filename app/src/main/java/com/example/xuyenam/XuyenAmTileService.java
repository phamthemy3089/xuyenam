package com.example.xuyenam;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.util.Log;


public class XuyenAmTileService  extends TileService {

    //region VARIABLE ---------------------------------------------------------------------------------------------------------------------------------------------------------
    private Tile tile =null;
    private BroadcastReceiver TitleReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive (Context context, Intent intent){
            String Action = intent.getAction();
            tile = getQsTile();

            Log.i("XuyenAmTileService.BroadcastReceiver.Action", Action);
            switch (Action)
            {
                case XuyenAmAction.SET_TITLE_INACTIVE_ACTION :
                    tile.setState(Tile.STATE_INACTIVE);
                        Log.i("XuyenAmTileService", "unregisterReceiver TitleReceiver ");
                        unregisterReceiver(TitleReceiver);


                    break;
                case XuyenAmAction.SET_TITLE_ACTIVE_ACTION :
                    tile.setState(Tile.STATE_ACTIVE);
                    break;
                default:
                    break;
            }

            tile.updateTile();
        }
    };
    //endregion---------------------------------------------------------------------------------------------------------------------------------------------------------


    //region PUBLIC OVERRIDE FUNCTIONS -------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void onStartListening() {
        Log.i("XuyenAmTileService", "onStartListening");
        super.onStartListening();
    }

    @Override
    public void onStopListening() {
        Log.i("XuyenAmTileService", "onStopListening");
        super.onStopListening();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("XuyenAmTileService", "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        Log.i("XuyenAmTileService", "onCreate");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(XuyenAmAction.SET_TITLE_ACTIVE_ACTION);
        intentFilter.addAction(XuyenAmAction.SET_TITLE_INACTIVE_ACTION);
        this.registerReceiver(TitleReceiver, intentFilter);
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.i("XuyenAmTileService", "onDestroy");
        unregisterReceiver(TitleReceiver);
        super.onDestroy();
    }

    @Override
    public void onTileAdded() {
        Log.i("XuyenAmTileService", "onTileAdded");
        tile = getQsTile();
        tile.setState( Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    @Override
    public void onTileRemoved() {
        Log.i("XuyenAmTileService", "onTileRemoved");
        Intent intent = new Intent(XuyenAmTileService.this, XuyenAmSerVice.class);
        stopService(intent);
            Log.i("XuyenAmTileService", "unregisterReceiver TitleReceiver ");
            this.unregisterReceiver(TitleReceiver);
    }

    @Override
    public void onClick() {
        if (tile.getState()== Tile.STATE_ACTIVE)
        {
            Log.i("XuyenAmTileService", "onClick STATE_INACTIVE");
            Intent intent = new Intent(XuyenAmTileService.this, XuyenAmSerVice.class);
            stopService(intent);

        }
        else
        {
            Log.i("XuyenAmTileService", "onClick STATE_ACTIVE");

            Intent intent = new Intent(XuyenAmTileService.this, XuyenAmSerVice.class);
            startService(intent);
            Log.i("XuyenAmTileService", "startService");


        }
        VibratingN();
    }
    //endregion

    //region PRIVATE FUNCTIONS -------------------------------------------------------------------------------------------------------------------------------------------------
    private void VibratingN()
    {
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(VibrationEffect.createOneShot(1000,100));
    }

    //endregion -------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
