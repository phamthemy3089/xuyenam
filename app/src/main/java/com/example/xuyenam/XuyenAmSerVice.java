package com.example.xuyenam;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class XuyenAmSerVice  extends Service{

    private AudioManager am = null;
    private final int Delaying = 99999000; // time auto Off SerVice (milisec)
    private AudioRecord record = null;
    private AudioTrack track = null;
    private String Mes ;
    private enum XuyenAmSerViceState {Not_Connect,Connect_To_Local,Connect_to_bluetooth};
    private XuyenAmSerViceState CurentState = XuyenAmSerViceState.Not_Connect;

    private Timer timer = new Timer();
    private  BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mConnectedHeadset;
    private BluetoothHeadset mBluetoothHeadset;
    private class  StopServiceTask extends TimerTask{

        @Override
        public void run() {
            if (CurentState == XuyenAmSerViceState.Not_Connect) {
                Mes = "Stopping SerVice !";
                Log.i("StopServiceTask", Mes);
                Toast.makeText(getApplicationContext(), Mes, Toast.LENGTH_LONG).show();
                unregisterReceiver(XuyenAmReceiver);
               stopSelf();
           }
        }
    };

    BluetoothProfile.ServiceListener mHeadsetProfileListener = new BluetoothProfile.ServiceListener()
    {

        /**
         * This method is never called, even when we closeProfileProxy on onPause.
         * When or will it ever be called???
         */
        @Override
        public void onServiceDisconnected(int profile)
        {
            mBluetoothHeadset.stopVoiceRecognition(mConnectedHeadset);
            unregisterReceiver(BluetoothReceiver);
            mBluetoothHeadset = null;
        }

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy)
        {
            // mBluetoothHeadset is just a head set profile,
            // it does not represent a head set device.
            mBluetoothHeadset = (BluetoothHeadset) proxy;

            // If a head set is connected before this application starts,
            // ACTION_CONNECTION_STATE_CHANGED will not be broadcast.
            // So we need to check for already connected head set.
            List<BluetoothDevice> devices = mBluetoothHeadset.getConnectedDevices();
            if (devices.size() > 0)
            {
                // Only one head set can be connected at a time,
                // so the connected head set is at index 0.
                mConnectedHeadset = devices.get(0);

                String log;

                // The audio should not yet be connected at this stage.
                // But just to make sure we check.
                if (mBluetoothHeadset.isAudioConnected(mConnectedHeadset))
                {
                    log = "Profile listener audio already connected"; //$NON-NLS-1$
                }
                else
                {
                    // The if statement is just for debug. So far startVoiceRecognition always
                    // returns true here. What can we do if it returns false? Perhaps the only
                    // sensible thing is to inform the user.
                    // Well actually, it only returns true if a call to stopVoiceRecognition is
                    // call somewhere after a call to startVoiceRecognition. Otherwise, if
                    // stopVoiceRecognition is never called, then when the application is restarted
                    // startVoiceRecognition always returns false whenever it is called.
                    if (mBluetoothHeadset.startVoiceRecognition(mConnectedHeadset))
                    {
                        log = "Profile listener startVoiceRecognition returns true"; //$NON-NLS-1$
                    }
                    else
                    {
                        log = "Profile listener startVoiceRecognition returns false"; //$NON-NLS-1$
                    }
                }

                Log.d("ServiceListener.onServiceConnected", log);
            }

            // During the active life time of the app, a user may turn on and off the head set.
            // So register for broadcast of connection states.
            registerReceiver(BluetoothReceiver,
                    new IntentFilter(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED));

            // Calling startVoiceRecognition does not result in immediate audio connection.
            // So register for broadcast of audio connection states. This broadcast will
            // only be sent if startVoiceRecognition returns true.
            IntentFilter f = new IntentFilter(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED);
            f.setPriority(Integer.MAX_VALUE);
            registerReceiver(BluetoothReceiver, f);
        }
    };


    private BroadcastReceiver BluetoothReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String Action = intent.getAction();
            Log.i("BroadcastReceiver.Action", Action);
            int state;
            String log = "";
            switch (Action)
            {
                case BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED :
                    StopSteaming();
                    //previousState  = intent.getIntExtra(BluetoothHeadset.EXTRA_PREVIOUS_STATE, BluetoothHeadset.STATE_DISCONNECTED);
                    state = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, BluetoothHeadset.STATE_DISCONNECTED);
                    if (state == BluetoothHeadset.STATE_CONNECTED)
                    {
                        mConnectedHeadset = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        // Audio should not be connected yet but just to make sure.
                        if (mBluetoothHeadset.isAudioConnected(mConnectedHeadset))
                        {
                            log = "Headset connected audio already connected";
                        }
                        else
                        {

                            // Calling startVoiceRecognition always returns false here,
                            // that why a count down timer is implemented to call
                            // startVoiceRecognition in the onTick and onFinish.
                            if (mBluetoothHeadset.startVoiceRecognition(mConnectedHeadset))
                            {
                                log = "Headset connected startVoiceRecognition returns true"; //$NON-NLS-1$
                            }
                            else
                            {
                                log = "Headset connected startVoiceRecognition returns false";
                            }
                        }
                    }
                    else if (state == BluetoothHeadset.STATE_DISCONNECTED)
                    {
                        log = "Headset connected audio STATE_DISCONNECTED";
                        // Calling stopVoiceRecognition always returns false here
                        // as it should since the headset is no longer connected.
                        mConnectedHeadset = null;
                    }
                    Log.i("BroadcastReceiver.ACTION_CONNECTION_STATE_CHANGED", log);
                    break;
                case BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED :
                    //previousState  = intent.getIntExtra(BluetoothHeadset.EXTRA_PREVIOUS_STATE, BluetoothHeadset.STATE_DISCONNECTED);

                    if (CurentState==XuyenAmSerViceState.Not_Connect) {
                        log ="StartSteaming";
                        StartSteaming();
                    }
                    else
                    {
                        log ="StopSteaming";
                        StopSteaming();
                    }
                    Log.i("BroadcastReceiver.ACTION_AUDIO_STATE_CHANGED", log);
                        setResultCode(Activity.RESULT_CANCELED);

                    break;
                default:
                    break;

            }
        }
    };

    private BroadcastReceiver XuyenAmReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String Action = intent.getAction();
            Log.i("BroadcastReceiver.Action", Action);
            switch (Action)
            {
                case XuyenAmAction.STOP_STREAM_ACTION :
                    StopSteaming();
                    break;
                case XuyenAmAction.START_STREAM_ACTION :
                    StartSteaming();
                    break;
                default:
                    break;

            }
        }
    };


    @Override
    public void onCreate() {
        Log.i("XuyenAmSerVice", "onCreate");
        am = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        am.setMode(AudioManager.MODE_IN_COMMUNICATION);


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i("XuyenAmSerVice", "onDestroy");
        StopSteaming();
        sendBroadcast(new Intent().setAction(XuyenAmAction.SET_TITLE_INACTIVE_ACTION));
        Log.i("sendBroadcast", "SET_TITLE_INACTIVE_ACTION");
        this.unregisterReceiver(XuyenAmReceiver);
        timer.cancel();
        timer.purge();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("XuyenAmSerVice", "onStartCommand");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (am.isBluetoothScoAvailableOffCall())
        {
            Log.i("XuyenAmSerVice", "isBluetoothScoAvailableOffCall");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            {
                Log.i("XuyenAmSerVice", "getProfileProxy");
                mBluetoothAdapter.getProfileProxy(this, mHeadsetProfileListener, BluetoothProfile.HEADSET);

            }
        }
        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel("XuyenAmSerVice2", "MyXuyenAmSerVice");
        }
        Notification notification = new Notification.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Much longer text that cannot fit one line...")
                .build();
        startForeground(2001, notification);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(XuyenAmAction.START_STREAM_ACTION);
            intentFilter.addAction(XuyenAmAction.STOP_STREAM_ACTION);
            intentFilter.setPriority(Integer.MAX_VALUE);
            this.registerReceiver(this.XuyenAmReceiver, intentFilter);

        StopServiceTask StopService = new StopServiceTask();
        timer.cancel();
        timer.purge();
        timer = new Timer();
        timer.schedule(StopService, Delaying);
        sendBroadcast(new Intent().setAction(XuyenAmAction.SET_TITLE_ACTIVE_ACTION));
        Log.i("sendBroadcast", "SET_TITLE_ACTIVE_ACTION");
        return START_NOT_STICKY;
    }

    //region Private
    private void StartSteaming()
    {
        timer.cancel();
        timer.purge();
        if (CurentState == XuyenAmSerViceState.Not_Connect) {
        boolean isConnectToBluetooth = false;
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter!=null &&mBluetoothAdapter.isEnabled() && BluetoothProfile.STATE_CONNECTED == mBluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
            Mes ="Connect Bluetooth HEADSET !";
            Toast.makeText(getApplicationContext(), Mes, Toast.LENGTH_LONG).show();
            Log.i("StartSteaming", Mes);
            am.setSpeakerphoneOn(false);
            am.startBluetoothSco();
            am.setBluetoothScoOn(true);
            isConnectToBluetooth = true;
        } else {
            Mes ="Not connect!";
            Toast.makeText(getApplicationContext(), Mes, Toast.LENGTH_LONG).show();
            Log.i("StartSteaming", Mes);
            am.setSpeakerphoneOn(true);
            am.stopBluetoothSco();
            am.setBluetoothScoOn(false);
            isConnectToBluetooth = false;
        }
         int minBufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
         record = new AudioRecord(MediaRecorder.AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,
                    minBufferSize);
        if (AcousticEchoCanceler.isAvailable()) {
            AcousticEchoCanceler echoCanceler = AcousticEchoCanceler.create(record.getAudioSessionId());
            echoCanceler.setEnabled(true);
        }
        int maxJitter = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, maxJitter,
                AudioTrack.MODE_STREAM);

            if (isConnectToBluetooth)
                CurentState = XuyenAmSerViceState.Connect_to_bluetooth;
            else
                CurentState = XuyenAmSerViceState.Connect_To_Local;

            //Log.i("StartSteaming: XuyenAmSerViceState", CurentState.toString());
            new Thread((new Runnable() {
                @Override
                public void run() {
                    recordAndPlay();
                }
            })).start();
            record.startRecording();
            track.play();
        }
    }

    private void StopSteaming()
    {

        if (CurentState!=XuyenAmSerViceState.Not_Connect) {
            record.stop();
            track.pause();
            CurentState=XuyenAmSerViceState.Not_Connect;
            StopServiceTask StopService = new StopServiceTask();
            timer = new Timer();
            timer.schedule(StopService, Delaying);
        }
    }

    private void recordAndPlay()
    {
        short[] lin = new short[1024];
        int num = 0;
        am.setMode(AudioManager.MODE_IN_COMMUNICATION);
        while (CurentState!=XuyenAmSerViceState.Not_Connect)
        {
            num = record.read(lin, 0, 1024);
            track.write(lin, 0, num);
        }
    }
    private String createNotificationChannel(String channelId, String channelName)
    {
        NotificationChannel chan = new NotificationChannel(channelId,channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.RED);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

    //endregion



}
