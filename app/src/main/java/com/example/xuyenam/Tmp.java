package com.example.xuyenam;

import android.app.IntentService;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


public class Tmp extends Service {
    private AudioManager am = null;
    private AudioRecord record = null;
    private AudioTrack track = null;
    private  Thread Th1 ;
    private int count = 0;
    private  int MAX_ATTEMPTS_TO_CONNECT = 3;
    private boolean isRecording = false;
    private static final String ACTION="android.bluetooth.BluetoothDevice.ACTION_ACL_CONNECTED";
    private BroadcastReceiver yourReceiver;



    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(getApplicationContext(), "Service onCreate", Toast.LENGTH_LONG).show();
        am = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        final int minBufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        this.yourReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (bluetoothAdapter.isEnabled() && BluetoothProfile.STATE_CONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
                    int state = intent.getIntExtra(
                            AudioManager.EXTRA_SCO_AUDIO_STATE, -1);
                    if (AudioManager.SCO_AUDIO_STATE_CONNECTED == state) {
                        Toast.makeText(getApplicationContext(), "Bluetooth HEADSET is CONNECTED", Toast.LENGTH_LONG).show();
                        Log.i("BroadcastReceiver", "onReceive: Bluetooth HEADSET is CONNECTED");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Bluetooth is OFF. Recording from Phone MIC", Toast.LENGTH_LONG).show();
                    Log.i("BroadcastReceiver", "onReceive: Bluetooth is OFF. Recording from Phone MIC");
                }

                record = new AudioRecord(MediaRecorder.AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT,
                        minBufferSize);
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        // intentFilter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED);
        intentFilter.addAction(ACTION);
        this.registerReceiver(this.yourReceiver, intentFilter);
        am.setMode(AudioManager.MODE_IN_COMMUNICATION);
        am.startBluetoothSco();
        if (record == null) {

            record = new AudioRecord(MediaRecorder.AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT,
                    minBufferSize);
        }
        if (AcousticEchoCanceler.isAvailable()) {
            AcousticEchoCanceler echoCanceler = AcousticEchoCanceler.create(record.getAudioSessionId());
            echoCanceler.setEnabled(true);
        }
        int maxJitter = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, maxJitter,
                AudioTrack.MODE_STREAM);
        am.setSpeakerphoneOn(true);
    }
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        // super.onStartCommand(intent,flags,startId);
        Toast.makeText(getApplicationContext(), "Service onStartCommand running", Toast.LENGTH_LONG).show();
        if (!isRecording) {
            isRecording = true;
            new Thread((new Runnable() {
                @Override
                public void run() {
                    recordAndPlay();
                }
            })).start();
            record.startRecording();
            track.play();
        }
        return Service.START_REDELIVER_INTENT;
    }



    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "Service onDestroy", Toast.LENGTH_LONG).show();
        if (isRecording) {
            record.stop();
            track.pause();
            isRecording = false;
        }
        this.unregisterReceiver(yourReceiver);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void recordAndPlay()
    {
        short[] lin = new short[1024];
        int num = 0;
        am.setMode(AudioManager.MODE_IN_COMMUNICATION);
        while (isRecording)
        {
            num = record.read(lin, 0, 1024);
            track.write(lin, 0, num);
        }
    }
}
