package com.example.xuyenam;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView text;
    public static final int REQUEST_CODE = (int) new Date().getTime();
    private List<String> lstPer =new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lstPer.add(Manifest.permission.RECORD_AUDIO);
        lstPer.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        lstPer.add(Manifest.permission.BLUETOOTH);
        lstPer.add(Manifest.permission.BLUETOOTH_ADMIN);
        lstPer.add(Manifest.permission.BLUETOOTH_PRIVILEGED);
        lstPer.add(Manifest.permission.CAPTURE_AUDIO_OUTPUT);
        lstPer.add(Manifest.permission.REQUEST_COMPANION_RUN_IN_BACKGROUND);
        lstPer.add(Manifest.permission.REQUEST_COMPANION_USE_DATA_IN_BACKGROUND);
        lstPer.add(Manifest.permission.WAKE_LOCK);
        lstPer.add(Manifest.permission.VIBRATE);
        CheckAndAddPermission(lstPer);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.textView);
    }



    public void startRecordAndPlay(View v)
    {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isEnabled() && BluetoothProfile.STATE_CONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET))
        {
            text.setText("HEADSET Connect");
        }
        else
        {
            text.setText("HEADSET Disonnect");
        }
    }

    public void stopRecordAndPlay(View v)
    {

    }


    public  void action_startservice (View V)
    {
        Log.i("MainActivity", "action_startservice");
        Intent intent = new Intent();
        intent.setAction(XuyenAmAction.START_STREAM_ACTION);
        sendBroadcast(intent);
    }

    public  void action_stopservice (View V)
    {
        Log.i("MainActivity", "action_stopservice");
        Intent intent = new Intent();
        intent.setAction(XuyenAmAction.STOP_STREAM_ACTION);
        sendBroadcast(intent);
    }


    private void CheckAndAddPermission(List<String> permissions)
    {
        List<String> permissionsAdd  =new ArrayList<String>();

        for (String per :permissions) {
            if (ActivityCompat.checkSelfPermission(this, per)
                    != PackageManager.PERMISSION_GRANTED)
            {
                permissionsAdd.add(per);
            }
        }
        if (permissionsAdd.size() >0) {
            ActivityCompat.requestPermissions(this, permissionsAdd.toArray(new String[0]),
                    10);
        }
    }
}
