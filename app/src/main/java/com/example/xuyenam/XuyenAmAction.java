package com.example.xuyenam;

import androidx.appcompat.app.AppCompatActivity;

public class XuyenAmAction {
    public static final String START_STREAM_ACTION = "com.example.foo.bar.STARTSTREAM";
    public static final String STOP_STREAM_ACTION = "com.example.foo.bar.STOPSTREAM";
    public static final String SET_TITLE_INACTIVE_ACTION = "com.example.foo.bar.SET_TITLE_INACTIVE_ACTION";
    public static final String SET_TITLE_ACTIVE_ACTION = "com.example.foo.bar.SET_TITLE_ACTIVE_ACTION";
}
